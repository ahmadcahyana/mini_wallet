from django.core.exceptions import ValidationError as DjangoValidationError
from django.http import JsonResponse
from rest_framework.exceptions import ValidationError as DRFValidationError
from rest_framework.views import set_rollback


def exception_handler(exc, context):
    if isinstance(exc, DjangoValidationError):
        exc = DRFValidationError(detail=exc.message)
        set_rollback()
        return response(exc.status_code, message=exc.detail[0])
    return None


def response(code=None, data=None, message='', status='', errors=None,
             sys_errors=None, extract_message=False):
    if errors is None:
        errors = []
    if data is None:
        data = []

    res_errors = []
    if errors:
        res_errors = decode_errors(errors)
    elif sys_errors:
        message = sys_errors

    if extract_message:
        message = message.strip().rstrip(".")
        add_message = ["{}: {}".format(i['field'], i['message']) for i in
                       res_errors]
        add_message = "; ".join(add_message)
        message = "{}. {}".format(message, add_message)

    if not message and errors:
        for x in res_errors:
            message = x.get('message')
    data = data or message or res_errors

    res = {
        'status': status,
        'data': data
    }
    return JsonResponse(res, status=code)


def parse_error_api(errors):
    messages = ""
    for key, val in errors.items():
        for item in val:
            if key != 'non_field_errors':
                messages = "{}: {}".format(key, item)
            else:
                messages = "{}".format(item)
            break
        break
    return messages


def decode_errors(data_errors):
    errors = []
    if data_errors:
        if isinstance(data_errors, list):
            for error_detail in data_errors:
                for key, value in error_detail.items():
                    error = extract_error(value, key)
                    errors.append(error)
        else:
            for key, value in data_errors.items():
                error = extract_error(value, key)
                errors.append(error)

    return errors


def extract_error(value, key):
    if isinstance(value, list):
        message = str(value[0])
        return {
            "field": key,
            "message": message
        }
    return None
