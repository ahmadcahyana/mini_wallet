import random
import string
from functools import wraps

from src.helper.response import response
from src.v1.customers.models import Customer


def generate_token(length):
    letters = string.ascii_lowercase + string.digits
    return ''.join(random.choice(letters) for i in range(length))


def authorized(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            auth_token = args[1].headers.get('Authorization', '')
            if auth_token:
                token = auth_token.split()[-1]
                customer = Customer.objects.get(token=token)
                data = {
                    'customer': customer
                }
                if not customer:
                    return response(
                        data={"status": 'fail',
                              'error': "Customer Does not exist!"})
            else:
                return response(data={'error': 'Not Authorized'})
        except Exception as e:
            return response(status='fail', data={'error': e.args[0]})
        return func(data, *args, **kwargs)

    return wrapper
