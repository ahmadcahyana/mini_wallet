from rest_framework import serializers

from src.v1.customers.models import Customer


class CustomerSerializer(serializers.Serializer):
    customer_xid = serializers.CharField(required=True)
    token = serializers.CharField()

    def __init__(self, *args, **kwargs):
        super(CustomerSerializer, self).__init__(*args, **kwargs)
        message = u"Missing data for required field."
        self.fields['customer_xid'].error_messages['blank'] = message
        self.fields['customer_xid'].error_messages['required'] = message
        self.fields['customer_xid'].error_messages['null'] = message

    def create(self, validated_data):
        return Customer.objects.create(**validated_data)
