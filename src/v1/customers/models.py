from django.db import models


class Customer(models.Model):
    customer_xid = models.CharField(max_length=255, primary_key=True,
                                    unique=True)
    token = models.CharField(max_length=255, blank=True)
