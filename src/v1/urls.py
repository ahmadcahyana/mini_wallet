from django.urls import path

from src.v1.wallet.views import InitWalletView, WalletView, \
    DepositWalletView, WithdrawWalletView

app_name = 'v1'
urlpatterns = [
    path('init', InitWalletView.as_view(), name='init'),
    path('wallet',
         WalletView.as_view({'get': 'get', 'post': 'post', 'patch': 'patch'}),
         name='wallet'),
    path('wallet/deposits', DepositWalletView.as_view(), name='deposits'),
    path('wallet/withdrawals', WithdrawWalletView.as_view(), name='withdraw'),
]
