from django.utils import timezone
from rest_framework import serializers

from src.v1.customers.models import Customer
from src.v1.wallet.models import Wallet, Deposit, Withdraw


class WalletSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=100, read_only=True)
    owned_by = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all())
    status = serializers.SerializerMethodField()
    enabled_at = serializers.DateTimeField(required=False)
    balance = serializers.FloatField(default=0.00)

    class Meta:
        model = Wallet
        fields = ['id', 'owned_by', 'status', 'enabled_at', 'balance']

    def get_status(self, data):
        if data.status:
            return 'enabled'
        else:
            return 'disabled'


class DepositSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=100, read_only=True)
    wallet = serializers.PrimaryKeyRelatedField(queryset=Wallet.objects.all())
    deposited_by = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all())
    status = serializers.BooleanField(default=True)
    deposited_at = serializers.DateTimeField(default=timezone.now())
    amount = serializers.FloatField(default=0.00)
    reference_id = serializers.CharField(max_length=100, required=True)

    class Meta:
        model = Deposit
        fields = ['id', 'wallet', 'deposited_by', 'status', 'deposited_at',
                  'amount', 'reference_id']

    def get_status(self, data):
        if data:
            return 'success'
        else:
            return 'failed'


class WithdrawSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=100, read_only=True)
    wallet = serializers.PrimaryKeyRelatedField(queryset=Wallet.objects.all())
    withdrawn_by = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all())
    status = serializers.BooleanField(default=True)
    withdrawn_at = serializers.DateTimeField(default=timezone.now())
    amount = serializers.FloatField(default=0.00)
    reference_id = serializers.CharField(max_length=100, required=True)

    class Meta:
        model = Withdraw
        fields = ['id', 'wallet', 'withdrawn_by', 'status', 'withdrawn_at',
                  'amount', 'reference_id']

    def get_status(self, data):
        if data:
            return 'success'
        else:
            return 'failed'
