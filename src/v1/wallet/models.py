import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from src.v1.customers.models import Customer


class Wallet(models.Model):
    id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4,
                          unique=True)
    owned_by = models.OneToOneField(Customer, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    enabled_at = models.DateTimeField(null=True)
    balance = models.FloatField(default=0.00)

    def __str__(self):
        return self.id


class Deposit(models.Model):
    id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4,
                          unique=True)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    deposited_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    deposited_at = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(default=0.00)
    reference_id = models.CharField(max_length=100, default=uuid.uuid4,
                                    unique=True)


class Withdraw(models.Model):
    id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4,
                          unique=True)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    withdrawn_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    withdrawn_at = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(default=0.00)
    reference_id = models.CharField(max_length=100, default=uuid.uuid4,
                                    unique=True)


@receiver(post_save, sender=Deposit)
def save_deposit_wallet(sender, instance, created, **kwargs):
    if created:
        before_balance = instance.wallet.balance
        total = before_balance + instance.amount
        instance.wallet.balance = total
        instance.wallet.save()


@receiver(post_save, sender=Withdraw)
def save_withdraw_wallet(sender, instance, created, **kwargs):
    if created:
        before_balance = instance.wallet.balance
        total = before_balance - instance.amount
        instance.wallet.balance = total
        instance.wallet.save()
