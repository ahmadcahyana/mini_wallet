from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from src.helper.response import response
from src.helper.utils import generate_token, authorized
from src.v1.customers.models import Customer
from src.v1.customers.serializers import CustomerSerializer
from src.v1.wallet.models import Wallet, Deposit, Withdraw
from src.v1.wallet.serializers import WalletSerializer, DepositSerializer, \
    WithdrawSerializer


class InitWalletView(APIView):
    queryset = Wallet.objects.all()
    serializer_class = WalletSerializer

    def post(self, request, format=None):
        customer_xid = request.data.get('customer_xid', '')
        customers = Customer.objects.filter(customer_xid=customer_xid)
        if not customers:
            try:
                customer = {
                    'customer_xid': customer_xid,
                    'token': generate_token(40)
                }

                customer_serializer = CustomerSerializer(data=customer)
                if not customer_serializer.is_valid():
                    return response(
                        data={'error': customer_serializer.errors},
                        status='fail')
                customer_serializer.save()
                wallet = {
                    'status': False,
                    'owned_by': customer_serializer.data['customer_xid']
                }
                wallet_serializer = WalletSerializer(data=wallet, many=False)
                if not wallet_serializer.is_valid():
                    return response(
                        data={'error': wallet_serializer.errors},
                        status='fail')
                wallet_serializer.save()
                data = {
                    'token': customer_serializer.data['token']
                }
                return response(data={"wallet": data}, status='success',
                                code=200)
            except Exception as e:
                return response(errors=e, status='fail', code=400)
        data = {"error": {"customer_xid": ["Duplicate entry."]}}
        return response(data=data, status='fail', code=400)


class WalletView(GenericViewSet):
    queryset = Wallet.objects.all()
    serializer_class = WalletSerializer

    @authorized
    def post(self, request, format=None):
        customer = self.get('customer')
        if customer:
            if customer.wallet.status:
                data = {"error": "Already enabled"}
                return response(data=data, status='fail', code=200)
            else:
                customer.wallet.enabled_at = timezone.now()
                customer.wallet.status = True
                customer.wallet.save()
                data = WalletSerializer(customer.wallet).data
                return response(data={"wallet": data}, status='success')
        return response(data={"error": "Data not found."}, status='fail',
                        code=400)

    @authorized
    def get(self, request, format=None):
        customer = self.get('customer')
        if customer:
            if customer.wallet.status:
                data = WalletSerializer(customer.wallet).data
                return response(data={"wallet": data}, status='success',
                                code=200)
            else:
                data = {"error": "Disabled"}
                return response(data=data, status='fail', code=400)
        return response(data={"error": "Data not found."}, status='fail',
                        code=400)

    @authorized
    def patch(self, request, *args, **kwargs):
        try:
            instance = self.get('customer')
            status = request.request.data.get('is_disabled') == 'true'
            instance.wallet.status = status
            instance.wallet.save()
            data = WalletSerializer(instance=instance.wallet).data
            return response(data={"wallet": data}, status='success', code=200)
        except Exception as e:
            return response(data={"error": [error for error in e.args]},
                            status='fail', code=400)


class DepositWalletView(APIView):
    queryset = Deposit.objects.all()
    serializer_class = DepositSerializer

    @authorized
    def post(self, request, format=None):
        customer = self.get('customer')
        amount = request.request.data.get('amount', '')
        reference_id = request.request.data.get('reference_id', '')
        if customer and amount and reference_id:
            deposit = {
                'wallet': customer.wallet.pk,
                'deposited_by': customer.pk,
                'amount': amount,
                'reference_id': reference_id
            }
            serializer = DepositSerializer(data=deposit)
            try:
                if not serializer.is_valid():
                    return response(data={'error': serializer.errors},
                                    status='fail', code=400)
                serializer.save(wallet=customer.wallet)
                return response(data={"deposit": serializer.data},
                                status='success', code=200)
            except Exception as e:
                return response(data={"error": [error for error in e.args]},
                                status='fail', code=400)
        return response(data={"error": "Please input data correctly"},
                        status='fail', code=400)


class WithdrawWalletView(APIView):
    queryset = Withdraw.objects.all()
    serializer_class = WithdrawSerializer

    @authorized
    def post(self, request, format=None):
        customer = self.get('customer')
        amount = request.request.data.get('amount', '')
        reference_id = request.request.data.get('reference_id', '')
        if customer and amount and reference_id:
            deposit = {
                'wallet': customer.wallet.pk,
                'withdrawn_by': customer.pk,
                'amount': amount,
                'reference_id': reference_id
            }
            serializer = WithdrawSerializer(data=deposit)
            try:
                if not serializer.is_valid():
                    return response(data={'error': serializer.errors},
                                    status='fail', code=400)
                serializer.save(wallet=customer.wallet)
                return response(data={"withdrawal": serializer.data},
                                status='success', code=200)
            except Exception as e:
                return response(data={"error": [error for error in e.args]},
                                status='fail', code=400)
        return response(data={"error": "Please input data correctly"},
                        status='fail', code=400)
