# MINI WALLET

## Usage

- Please install Python 3 before using this source
- Preferably use [virtualenv](https://virtualenv.pypa.io/en/latest/)
- install requirements `pip istall -r requirements.txt`
- migration `python manage.py migrate`
- run `python manage.py runserver 80` if you have permission error **Error: You
  don't have permission to access that port.**
  add sudo to the command `sudo python manage.py runserver 80`
